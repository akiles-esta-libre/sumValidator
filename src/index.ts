function twoNumberSum(array: number[], targetSum: number): number[] {

    let a: number;
    let b: number;
    let found: boolean = false;

    for (let i = 0; !found && i < array.length - 1; i++) {

        a = array[i];

        let resto = targetSum - a;

        let j = array.indexOf(resto);

        if (j > i) {
            b = array[j];
            found = true;
            break;
        }

    }

    if (found) {
        console.log(`El array resultante es: [${a}, ${b}]`)
        return [a, b];
    }
    else {
        console.log("No hay coincidencias");
        return [];
    }
}

const array = [3, 5, -4, 8, 11, 1, -1, 6]
const targetSum = 10
const result = twoNumberSum(array, targetSum)

// El array resultante es: [11, -1]
