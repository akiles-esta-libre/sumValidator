function twoNumberSum(array, targetSum) {
    var a;
    var b;
    var found = false;
    for (var i = 0; !found && i < array.length - 1; i++) {
        a = array[i];
        var resto = targetSum - a;
        var j = array.indexOf(resto);
        if (j > i) {
            b = array[j];
            found = true;
            break;
        }
    }
    if (found) {
        console.log("El array resultante es: [" + a + ", " + b + "]");
        return [a, b];
    }
    else {
        console.log("No hay coincidencias");
        return [];
    }
}
var array = [3, 5, -4, 8, 11, 1, -1, 6];
var targetSum = 10;
var result = twoNumberSum(array, targetSum);
// El array resultante es: [11, -1]
